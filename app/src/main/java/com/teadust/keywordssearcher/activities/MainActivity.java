package com.teadust.keywordssearcher.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.teadust.keywordssearcher.classes.TermFrequency;
import com.teadust.keywordssearcher.classes.TfIdf;
import com.teadust.keywordssearcher.classes.Word;
import com.teadust.keywordssearcher.classes.WordFrequency;
import com.teadust.keywordssearcher.classes.adapters.FrequencyAdapter;
import com.teadust.keywordssearcher.classes.adapters.ListViewAdapter;
import com.teadust.keywordssearcher.classes.async_tasks.GetAllWordsAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.ResultMapAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.StorageFileAsyncTask;
import com.teadust.keywordssearcher.fragments.AnalyseTextOneFragment;
import com.teadust.keywordssearcher.fragments.AnalyseTextTwoFragment;
import com.teadust.keywordssearcher.fragments.FrequencyFragment;
import com.teadust.keywordssearcher.R;
import com.teadust.keywordssearcher.fragments.FirstSourceTextFragment;
import com.teadust.keywordssearcher.classes.adapters.ViewPagerAdapter;
import com.teadust.keywordssearcher.fragments.SecondSourceTextFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    /**
     * Field for trapping bugs.
     * Delete in release version of application.
     */
    private static final String TAG = "TAG";

    /**
     * Result code of selected file in "File Manager".
     */
    private static final int DOC_ONE_RESULT_CODE = 1;
    private static final int DOC_TWO_RESULT_CODE = 2;
    private static final int SETTINGS_RESULT_CODE = 3;
    private static final int VOCABULARY_RESULT_CODE = 4;

    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;

    // ViewPager and its elements, that is two fragments,
    // where there are text and analysis of text.
    private ViewPager mViewPager;
    private ViewPagerAdapter mAdapter;
    private TabLayout mTabLayout;
    private Fragment mFirstSourceTextFragment;
    private Fragment mSecondSourceTextFragment;
    private Fragment mAnalyseTextOneFragment;
    private Fragment mAnalyseTextTwoFragment;
    private Fragment mFrequencyFragment;
    private List<String> mParticles;

    private List<String> mDocOne;
    private List<String> mDocTwo;

    private boolean mFlagOne = false;
    private boolean mFlagTwo = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeElementsId();
        setupElements();
    }

    private void initializeElementsId() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
    }

    private void setupElements() {
        setSupportActionBar(mToolbar);

        setupViewPager(mViewPager);

        mTabLayout.setupWithViewPager(mViewPager);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(navigationViewListener);
        mParticles = new ArrayList<String>();
        for (String str : ResultMapAsyncTask.particles) {
            mParticles.add(str);
        }
        Collections.sort(mParticles);
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        mFirstSourceTextFragment = new FirstSourceTextFragment();
        mSecondSourceTextFragment = new SecondSourceTextFragment();
        mAnalyseTextOneFragment = new AnalyseTextOneFragment();
        mAnalyseTextTwoFragment = new AnalyseTextTwoFragment();
        mFrequencyFragment = new FrequencyFragment();

        mAdapter.addFragment(mFirstSourceTextFragment, "Текст 1");
        mAdapter.addFragment(mSecondSourceTextFragment, "Текст 2");
        mAdapter.addFragment(mAnalyseTextOneFragment, "Анализ текста 1");
        mAdapter.addFragment(mAnalyseTextTwoFragment, "Анализ текста 2");
        mAdapter.addFragment(mFrequencyFragment, "Частотный анализ");

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(5);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case DOC_ONE_RESULT_CODE:
                    Uri uriFirstDoc = data.getData();

                    StorageFileAsyncTask firstDocTask = new StorageFileAsyncTask(
                            this,
                            uriFirstDoc,
                            ((FirstSourceTextFragment) mFirstSourceTextFragment).getTextView());
                    firstDocTask.execute();

                    mFlagOne = false;
                    mViewPager.setCurrentItem(0, true);
                    break;
                case DOC_TWO_RESULT_CODE:
                    Uri uriSecondDoc = data.getData();

                    StorageFileAsyncTask secondDocTask = new StorageFileAsyncTask(
                            this,
                            uriSecondDoc,
                            ((SecondSourceTextFragment) mSecondSourceTextFragment).getTextView());
                    secondDocTask.execute();

                    mFlagTwo = false;
                    mViewPager.setCurrentItem(1, true);
                    break;
                case SETTINGS_RESULT_CODE:
                    int textOneFontSize = data.getIntExtra(SettingsActivity.TEXT_ONE_FONT_SIZE, 20);
                    int textTwoFontSize = data.getIntExtra(SettingsActivity.TEXT_TWO_FONT_SIZE, 20);
                    ((FirstSourceTextFragment) mFirstSourceTextFragment).getTextView().setTextSize(textOneFontSize);
                    ((SecondSourceTextFragment) mSecondSourceTextFragment).getTextView().setTextSize(textTwoFontSize);
                    break;
            }
        }
    }

    /**
     * Listener for mNavigationView.
     * <br>Click events on menu items.
     * @see #mNavigationView
     */
    NavigationView.OnNavigationItemSelectedListener navigationViewListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            mDrawerLayout.closeDrawer(GravityCompat.START);

            switch (item.getItemId()) {
                case R.id.nav_menu_doc_open_1:
                    Intent firstDoc = new Intent(Intent.ACTION_GET_CONTENT);
                    firstDoc.setType("file/plain");
                    firstDoc.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(firstDoc, DOC_ONE_RESULT_CODE);
                    break;
                case R.id.nav_menu_doc_open_2:
                    Intent secondDoc = new Intent(Intent.ACTION_GET_CONTENT);
                    secondDoc.setType("file/plain");
                    secondDoc.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(secondDoc, DOC_TWO_RESULT_CODE);
                    break;
                case R.id.nav_menu_analyse_doc_one:
                    navMenuAnalyseDocONeClick();
                    mFlagOne = true;
                    mViewPager.setCurrentItem(2, true);
                    break;
                case R.id.nav_menu_analyse_doc_two:
                    navMenuAnalyseDocTwoClick();
                    mFlagTwo = true;
                    mViewPager.setCurrentItem(3, true);
                    break;
                case R.id.nav_menu_analyse_all:
//                    if (!mFlagOne && !mFlagTwo) {
//                        navMenuAnalyseDocONeClick();
//                        navMenuAnalyseDocTwoClick();
//                        mFlagOne = mFlagTwo = true;
//                    } else if (!mFlagOne) {
//                        navMenuAnalyseDocONeClick();
//                        mFlagOne = true;
//                    } else if (!mFlagTwo) {
//                        navMenuAnalyseDocTwoClick();
//                        mFlagTwo = true;
//                    }
//                    try {
//                        navMenuAnalyseDocAllClick();
//                    } catch (ExecutionException e) {
//                        e.printStackTrace();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    try {
                        navMenuAnalyseFrequencyClick();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mViewPager.setCurrentItem(4, true);
                    break;
                case R.id.nav_menu_vocabulary:
                    Intent vocIntent = new Intent(MainActivity.this, VocabularyActivity.class);
                    startActivityForResult(vocIntent, VOCABULARY_RESULT_CODE);
                    break;
                case R.id.nav_menu_settings:
                    Intent setIntent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivityForResult(setIntent, SETTINGS_RESULT_CODE);
                    break;
            }

            return true;
        }
    };

    private void navMenuAnalyseDocONeClick() {
        String textOne = getTextOne();

        LinkedHashMap<Integer, String> textOneMap = null;
        TermFrequency termFreqTextOne = new TermFrequency(textOne);
        try {
            textOneMap = termFreqTextOne.getResultAllWordsMap();
            mDocOne = termFreqTextOne.getAllWordsList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Word> textOneWords = new ArrayList<Word>();
        int countTextOne = 0;
        for (Map.Entry<Integer, String> entry : textOneMap.entrySet()) {
            textOneWords.add(new Word(countTextOne, entry.getKey(), entry.getValue()));
            countTextOne++;
        }

        sendDataToListViewTextOne(textOneWords);
    }

    private void navMenuAnalyseDocTwoClick() {
        String textTwo = getTextTwo();

        LinkedHashMap<Integer, String> textTwoMap = null;
        TermFrequency termFreqTextTwo = new TermFrequency(textTwo);
        try {
            textTwoMap = termFreqTextTwo.getResultAllWordsMap();
            mDocTwo = termFreqTextTwo.getAllWordsList();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<Word> textTwoWords = new ArrayList<Word>();
        int countTextTwo = 0;
        for (Map.Entry<Integer, String> entry : textTwoMap.entrySet()) {
            textTwoWords.add(new Word(countTextTwo, entry.getKey(), entry.getValue()));
            countTextTwo++;
        }

        sendDataToListViewTextTwo(textTwoWords);
    }

    private void navMenuAnalyseFrequencyClick() throws ExecutionException, InterruptedException {
        String textOne = getTextOne();
        String textTwo = getTextTwo();

        GetAllWordsAsyncTask allWords = new GetAllWordsAsyncTask(textOne);
        List<String> wordsListOne = allWords.execute().get();

        allWords = new GetAllWordsAsyncTask(textTwo);
        List<String> wordsListTwo = allWords.execute().get();

        List<WordFrequency> wordFreqOne = new ArrayList<WordFrequency>();
        TfIdf tfIdf = new TfIdf(wordsListOne, wordsListTwo, "Лориан");
        double result = tfIdf.getTfIdf();
        wordFreqOne.add(new WordFrequency(0, result, "Лориан"));

        result = tfIdf.tfIdf(wordsListOne, wordsListTwo, "Кристоф");
        wordFreqOne.add(new WordFrequency(1, result, "Кристоф"));

        result = tfIdf.tfIdf(wordsListOne, wordsListTwo, "Дарэл");
        wordFreqOne.add(new WordFrequency(2, result, "Дарэл"));

        result = tfIdf.tfIdf(wordsListOne, wordsListTwo, "Вивиан");
        wordFreqOne.add(new WordFrequency(3, result, "Вивиан"));

        result = tfIdf.tfIdf(wordsListOne, wordsListTwo, "очень");
        wordFreqOne.add(new WordFrequency(4, result, "очень"));

        result = tfIdf.tfIdf(wordsListOne, wordsListTwo, "Подросток");
        wordFreqOne.add(new WordFrequency(5, result, "Подросток"));

        List<WordFrequency> wordFreqTwo = new ArrayList<WordFrequency>();
        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "Лориан");
        wordFreqTwo.add(new WordFrequency(0, result, "Лориан"));

        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "Арчи");
        wordFreqTwo.add(new WordFrequency(1, result, "Арчи"));

        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "Кристоф");
        wordFreqTwo.add(new WordFrequency(2, result, "Кристоф"));

        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "очень");
        wordFreqTwo.add(new WordFrequency(3, result, "очень"));

        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "Дарэл");
        wordFreqTwo.add(new WordFrequency(4, result, "Дарэл"));

        result = tfIdf.tfIdf(wordsListTwo, wordsListOne, "глаза");
        wordFreqTwo.add(new WordFrequency(5, result, "глаза"));

        FrequencyAdapter adapterOne = new FrequencyAdapter(this, wordFreqOne);
        ((FrequencyFragment) mFrequencyFragment).getListViewOne().setAdapter(adapterOne);

        FrequencyAdapter adapterTwo = new FrequencyAdapter(this, wordFreqTwo);
        ((FrequencyFragment) mFrequencyFragment).getListViewTwo().setAdapter(adapterTwo);

//        List<WordFrequency> fList = new ArrayList<WordFrequency>();
//        int count = 0;
//        for (String str : wordsListOne) {
//            fList.add(new WordFrequency(count, 12, "asdkfj"));
//        }
//
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Лориан"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Кристоф"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Дарэл"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Вивиан"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "очень"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Подросток"));
//
//        count = 0;
//        List<WordFrequency> frequencyListTwo = new ArrayList<WordFrequency>();
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Лориан"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Арчи"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Кристоф"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "очень"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "Дарэл"));
//        wordsListOne.add(new WordFrequency(count++, 12.0, "глаза"));
    }

    private void navMenuAnalyseDocAllClick() throws ExecutionException, InterruptedException {
        String textOne = getTextOne();
        String textTwo = getTextTwo();

        List<String> textOneWords = null;
        List<String> textTwoWords = null;
        String term = "";

        TermFrequency termFrequency = new TermFrequency(textOne);
        textOneWords = termFrequency.getAllWordsList();
        termFrequency = new TermFrequency(textTwo);
        textTwoWords = termFrequency.getAllWordsList();


        TfIdf tfIdf = new TfIdf(textOneWords, textTwoWords, "Лориан");
        double result = tfIdf.getTfIdf();
        Log.d("double", result + " ");
    }

    /**
     * Temporary method. Remove when needn't.
     * @return Toast - "In development mode".
     */
    private void stub() {
        Toast.makeText(MainActivity.this, "In development mode", Toast.LENGTH_SHORT).show();
    }

    private String getTextOne() {
        String sourceText = ((FirstSourceTextFragment) mFirstSourceTextFragment).getTextView().getText().toString();
        return sourceText;
    }

    private String getTextTwo() {
        String sourceText = ((SecondSourceTextFragment) mSecondSourceTextFragment).getTextView().getText().toString();
        return sourceText;
    }

    private void sendDataToListViewTextOne(List<Word> wordsList) {
        ListViewAdapter adapter = new ListViewAdapter(this, wordsList);
        ((AnalyseTextOneFragment) mAnalyseTextOneFragment).getListView().setAdapter(adapter);
    }

    private void sendDataToListViewTextTwo(List<Word> wordsList) {
        ListViewAdapter adapter = new ListViewAdapter(this, wordsList);
        ((AnalyseTextTwoFragment) mAnalyseTextTwoFragment).getListView().setAdapter(adapter);
    }
}
