package com.teadust.keywordssearcher.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.teadust.keywordssearcher.R;

public class SettingsActivity extends AppCompatActivity {

    public static final String TEXT_ONE_FONT_SIZE = "text_one_back_color";
    public static final String TEXT_TWO_FONT_SIZE = "text_two_back_color";

    private static final int SMALL = 16;
    private static final int MIDDLE = 20;
    private static final int LARGE = 26;

    private Button mTextOneSmallSize;
    private Button mTextOneMiddleSize;
    private Button mTextOneLargeSize;
    private Button mTextOneReset;
    private Button mTextTwoSmallSize;
    private Button mTextTwoMiddleSize;
    private Button mTextTwoLargeSize;
    private Button mTextTwoReset;
    private Button mApply;

    private int mTextOneFontSize = 20;
    private int mTextTwoFontSize = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mTextOneSmallSize = (Button) findViewById(R.id.text_one_small);
        mTextOneSmallSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextOneFontSize = SMALL;
            }
        });

        mTextOneMiddleSize = (Button) findViewById(R.id.text_one_middle);
        mTextOneMiddleSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextOneFontSize = MIDDLE;
            }
        });

        mTextOneLargeSize = (Button) findViewById(R.id.text_one_large);
        mTextOneLargeSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextOneFontSize = LARGE;
            }
        });

        mTextTwoSmallSize = (Button) findViewById(R.id.text_two_small);
        mTextTwoSmallSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextTwoFontSize = SMALL;
            }
        });

        mTextTwoMiddleSize = (Button) findViewById(R.id.text_two_middle);
        mTextTwoMiddleSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextTwoFontSize = MIDDLE;
            }
        });

        mTextTwoLargeSize = (Button) findViewById(R.id.text_two_large);
        mTextTwoLargeSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextTwoFontSize = LARGE;
            }
        });

        mTextOneReset = (Button) findViewById(R.id.text_one_reset);
        mTextOneReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextOneFontSize = 20;
            }
        });

        mTextTwoReset = (Button) findViewById(R.id.text_two_reset);
        mTextTwoReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextOneFontSize = 20;
            }
        });

        mApply = (Button) findViewById(R.id.apply);
        mApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(TEXT_ONE_FONT_SIZE, mTextOneFontSize);
                intent.putExtra(TEXT_TWO_FONT_SIZE, mTextTwoFontSize);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
