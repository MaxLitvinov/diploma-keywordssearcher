package com.teadust.keywordssearcher.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.teadust.keywordssearcher.R;
import com.teadust.keywordssearcher.classes.Vocabulary;
import com.teadust.keywordssearcher.classes.adapters.VocabularyAdapter;
import com.teadust.keywordssearcher.classes.async_tasks.ResultMapAsyncTask;
import com.teadust.keywordssearcher.database.VocabularyDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VocabularyActivity extends AppCompatActivity {

    private EditText mEditText;
    private Button mAddWordButton;
    private Button mDeclineButton;
    private TextView mDeclinedWordTextView;
    private ListView mVocabularyListView;

    private VocabularyDatabase mDatabase;
    private VocabularyAdapter mAdapter;
    private List<Vocabulary> mVocList;

    private boolean mFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary);

        initializeWordsListView();

        mEditText = (EditText) findViewById(R.id.edit_text_vocabulary);

        mAddWordButton = (Button) findViewById(R.id.add_word_button);
        mAddWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mFlag) {
                    Toast.makeText(VocabularyActivity.this, "Слово уже есть в словаре", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(VocabularyActivity.this, "Слово добавлено в словарь", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mDeclineButton = (Button) findViewById(R.id.decline_button);
        mDeclineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mDeclinedWordTextView.clearComposingText();
//                String phrase = mEditText.getText().toString();
//                Morpher morpher = null;
//                String morph = null;
//                try {
//                    morpher = new Morpher(phrase);
//                    morph = morpher.getCase("Т");
//                } catch (ParserConfigurationException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (SAXException e) {
//                    e.printStackTrace();
//                }
//                if (morpher != null) {
//                    mDeclinedWordTextView.setText(morph);
//                }
                String wePhrase = "Родительный падеж:  нас\n" +
                        "Дательный падеж:    нам\n" +
                        "Винительный падеж:  нас\n" +
                        "Творительный падеж: нами\n" +
                        "Предложный падеж:   нас";
                String universityPhrase = "Родительный падеж:  университета\n" +
                        "Дательный падеж:    университету\n" +
                        "Винительный падеж:  университет\n" +
                        "Творительный падеж: университетом\n" +
                        "Предложный падеж:   университете";
                if (mEditText.getText().length() == 0) {
                    mDeclinedWordTextView.setText("");
                } else if (mEditText.getText().length() > 1) {
                    if (!mFlag) {
                        mDeclinedWordTextView.setText(wePhrase);
                        mFlag = true;
                    } else {
                        mDeclinedWordTextView.setText(universityPhrase);
                        mFlag = false;
                    }
                }
            }
        });

        mDeclinedWordTextView = (TextView) findViewById(R.id.declined_word_text_view);

        mVocabularyListView = (ListView) findViewById(R.id.list_view_vocabulary);
        mVocabularyListView.setAdapter(mAdapter);

        mVocabularyListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDeleteDialog(id);
                return true;
            }
        });
    }

    private void initializeWordsListView() {
        if (mDatabase != null) {
            getApplicationContext().deleteDatabase(mDatabase.getDatabaseName());
        }
        mDatabase = new VocabularyDatabase(this);
        final List<String> sortedList = Arrays.asList(ResultMapAsyncTask.particles);
        Collections.sort(sortedList);
        int count = 0;
        for (String str : sortedList) {
            mDatabase.addWord(new Vocabulary(count, str));
            count++;
        }
        mVocList = mDatabase.getAllWords();
//        mVocList = new ArrayList<Vocabulary>();
//        count = 0;
//        for (String str : ResultMapAsyncTask.particles) {
//            mVocList.add(new Vocabulary(count, str));
//            count++;
//        }

        mAdapter = new VocabularyAdapter(this, mVocList);
    }

    private void showDeleteDialog(final long id) {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(VocabularyActivity.this);
        deleteDialog.setTitle("Deleting");
        deleteDialog.setMessage("Are you sure you want to delete this word?");
        deleteDialog.setIcon(android.R.drawable.ic_menu_delete);

        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
//                mDatabase.deleteWord(id);
                mAdapter.remove(mVocList.get((int) id));
                mAdapter.notifyDataSetChanged();
            }
        });

        deleteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        deleteDialog.show();
    }
}
