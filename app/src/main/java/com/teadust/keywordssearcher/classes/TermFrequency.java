package com.teadust.keywordssearcher.classes;

import android.util.Log;

import com.teadust.keywordssearcher.classes.async_tasks.GetAllWordsAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.GetWordsMapAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.QuantityAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.ResultMapAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.ResultMapListAsyncTask;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class TermFrequency {

    private String mSourceText;
    private List<String> mWordsList;
    private LinkedHashMap<String, Integer> mWordsMap;
    private List<Integer> mQuantityList;
    private LinkedHashMap<Integer, String> mResultAllWordsMap;

    public TermFrequency(String sourceText) {
        mSourceText = sourceText;
    }

    public List<String> getAllWordsList() throws ExecutionException, InterruptedException {
        GetAllWordsAsyncTask wordsListTask = new GetAllWordsAsyncTask(mSourceText);
        mWordsList = wordsListTask.execute().get();
        return mWordsList;
    }

    public LinkedHashMap<Integer, String> getResultAllWordsMap() throws ExecutionException, InterruptedException {
        GetAllWordsAsyncTask wordsListTask = new GetAllWordsAsyncTask(mSourceText);
        mWordsList = wordsListTask.execute().get();

        GetWordsMapAsyncTask wordsMapTask = new GetWordsMapAsyncTask(mWordsList);
        mWordsMap = wordsMapTask.execute().get();
        for (Map.Entry<String, Integer> entry : mWordsMap.entrySet()) {
            Log.d("WM", entry.getKey() + ": " + entry.getValue());
        }

        QuantityAsyncTask quantityTask = new QuantityAsyncTask(mWordsMap);
        mQuantityList = quantityTask.execute().get();

        ResultMapAsyncTask resultMapTask = new ResultMapAsyncTask(mWordsMap, mQuantityList);
        mResultAllWordsMap = resultMapTask.execute().get();

        return mResultAllWordsMap;
    }
}
