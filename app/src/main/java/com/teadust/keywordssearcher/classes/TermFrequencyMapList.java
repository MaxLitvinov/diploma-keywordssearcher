package com.teadust.keywordssearcher.classes;

import android.util.Log;

import com.teadust.keywordssearcher.classes.async_tasks.GetAllWordsAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.GetWordsMapAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.QuantityAsyncTask;
import com.teadust.keywordssearcher.classes.async_tasks.ResultMapListAsyncTask;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class TermFrequencyMapList {

    private String mSourceText;
    private List<String> mWordsList;
    private LinkedHashMap<String, Integer> mWordsMap;
    private List<Integer> mQuantityList;
    private LinkedHashMap<Integer, List<String>> mResultAllWordsMapList;

    public TermFrequencyMapList(String sourceText) {
        mSourceText = sourceText;
    }

    public List<String> getAllWordsList() throws ExecutionException, InterruptedException {
        GetAllWordsAsyncTask wordsListTask = new GetAllWordsAsyncTask(mSourceText);
        mWordsList = wordsListTask.execute().get();
        return mWordsList;
    }

    public LinkedHashMap<Integer, List<String>> getResultAllWordsMap() throws ExecutionException, InterruptedException {
        GetAllWordsAsyncTask wordsListTask = new GetAllWordsAsyncTask(mSourceText);
        mWordsList = wordsListTask.execute().get();

        GetWordsMapAsyncTask wordsMapTask = new GetWordsMapAsyncTask(mWordsList);
        mWordsMap = wordsMapTask.execute().get();
        for (Map.Entry<String, Integer> entry : mWordsMap.entrySet()) {
            Log.d("WM", entry.getKey() + ": " + entry.getValue());
        }

        QuantityAsyncTask quantityTask = new QuantityAsyncTask(mWordsMap);
        mQuantityList = quantityTask.execute().get();

        ResultMapListAsyncTask resultMapTask = new ResultMapListAsyncTask(mWordsMap, mQuantityList);
        mResultAllWordsMapList = resultMapTask.execute().get();

        return mResultAllWordsMapList;
    }
}
