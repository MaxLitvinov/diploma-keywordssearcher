package com.teadust.keywordssearcher.classes;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TfIdf {

    private String mTerm;
    private List<String> mDocumentOne;
    private List<String> mDocumentTwo;

    public TfIdf(List<String> documentOne, List<String> documentTwo, String term) {
        mDocumentOne = documentOne;
        mDocumentTwo = documentTwo;
        mTerm = term;
    }

    private double tf(List<String> doc, String term) {
        int count = 0;
        for (String word : doc) {
            if (term.equalsIgnoreCase(word)) {
                count++;
            }
        }
        double result = (double) count / mDocumentOne.size();
        return result;
    }

    private double idf(List<String> docOne, List<String> docTwo, String term) {
        List<String> allDocs = new ArrayList<String>();
        allDocs.addAll(docOne);
        allDocs.addAll(docTwo);
        double n = 0;
        for (String word : allDocs) {
            if (term.equalsIgnoreCase(word)) {
                n++;
                break;
            }
        }
        double result = Math.log(allDocs.size() / n);
        return result;
    }

    public double tfIdf(List<String> docOne, List<String> docTwo, String term) {
        double tf = tf(docOne, term);
        double idf = idf(docOne, docTwo, term);
        double result = tf * idf;
        //result = round(result);
        return result;
    }

    private double round(double d) {
        DecimalFormat twoDecimals = new DecimalFormat("#.###");
        return Double.valueOf(twoDecimals.format(d));
    }

    public double getTf() {
        return tf(mDocumentOne, mTerm);
    }

    public double getTf(String term) {
        return tf(mDocumentOne, term);
    }

    public double getTf(List<String> doc, String term) {
        return tf(doc, term);
    }

    public double getIdf() {
        return idf(mDocumentOne, mDocumentTwo, mTerm);
    }

    public double getTfIdf() {
        return tfIdf(mDocumentOne, mDocumentTwo, mTerm);
    }
}
