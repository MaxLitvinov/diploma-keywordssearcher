package com.teadust.keywordssearcher.classes;

public class Vocabulary {

    private int mId;
    private String mWord;

    public Vocabulary() {
    }

    public Vocabulary(int id, String word) {
        mId = id;
        mWord = word;
    }

    public Vocabulary(String word) {
        mWord = word;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getWord() {
        return mWord;
    }

    public void setWord(String word) {
        mWord = word;
    }
}
