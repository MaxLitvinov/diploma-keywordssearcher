package com.teadust.keywordssearcher.classes;

public class Word {

    private int mId;
    private int mQuantity;
    private String mWord;

    public Word(int id, int quantity, String word) {
        mId = id;
        mQuantity = quantity;
        mWord = word;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public String getWord() {
        return mWord;
    }

    public void setWord(String word) {
        mWord = word;
    }
}
