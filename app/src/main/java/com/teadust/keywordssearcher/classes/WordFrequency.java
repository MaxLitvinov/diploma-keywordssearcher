package com.teadust.keywordssearcher.classes;

public class WordFrequency {

    private int mId;
    private double mFrequency;
    private String mWord;

    public WordFrequency(int id, double frequency, String word) {
        mId = id;
        mFrequency = frequency;
        mWord = word;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public double getFrequency() {
        return mFrequency;
    }

    public void setFrequency(double frequency) {
        mFrequency = frequency;
    }

    public String getWord() {
        return mWord;
    }

    public void setWord(String word) {
        mWord = word;
    }
}
