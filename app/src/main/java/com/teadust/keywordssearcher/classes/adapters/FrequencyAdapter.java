package com.teadust.keywordssearcher.classes.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teadust.keywordssearcher.R;
import com.teadust.keywordssearcher.classes.WordFrequency;

import java.util.List;
import java.util.Locale;

public class FrequencyAdapter extends ArrayAdapter<WordFrequency> {

    public FrequencyAdapter(Context context, List<WordFrequency> wordList) {
        super(context, R.layout.frequency_list, wordList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WordFrequency wordFrequency = getItem(position);
        FrequencyHolder frequencyHolder;
        if (convertView == null) {
            frequencyHolder = new FrequencyHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.frequency_list, parent, false);
            frequencyHolder.frequency = (TextView) convertView.findViewById(R.id.frequency);
            frequencyHolder.word = (TextView) convertView.findViewById(R.id.word);
            convertView.setTag(frequencyHolder);
        } else {
            frequencyHolder = (FrequencyHolder) convertView.getTag();
        }
        String word = wordFrequency.getWord();
        String quantity = String.format("%.3f", wordFrequency.getFrequency());
        frequencyHolder.word.setText(word);
        frequencyHolder.frequency.setText(quantity);
        return convertView;
    }

    private class FrequencyHolder {

        public TextView frequency;
        public TextView word;
    }
}