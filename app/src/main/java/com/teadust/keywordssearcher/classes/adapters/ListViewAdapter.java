package com.teadust.keywordssearcher.classes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teadust.keywordssearcher.R;
import com.teadust.keywordssearcher.classes.Word;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Word> {

    public ListViewAdapter(Context context, List<Word> wordList) {
        super(context, R.layout.words_list, wordList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Word wordList = getItem(position);
        WordsHolder wordsHolder;
        if (convertView == null) {
            wordsHolder = new WordsHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.words_list, parent, false);
            wordsHolder.word = (TextView) convertView.findViewById(R.id.word);
            wordsHolder.quantity = (TextView) convertView.findViewById(R.id.quantity);
            convertView.setTag(wordsHolder);
        } else {
            wordsHolder = (WordsHolder) convertView.getTag();
        }
        String word = wordList.getWord();
        String quantity = String.valueOf(wordList.getQuantity());
        wordsHolder.word.setText(word);
        wordsHolder.quantity.setText(quantity);
        return convertView;
    }

    private class WordsHolder {

        public TextView word;
        public TextView quantity;
    }
}