package com.teadust.keywordssearcher.classes.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.teadust.keywordssearcher.R;
import com.teadust.keywordssearcher.classes.Vocabulary;
import com.teadust.keywordssearcher.classes.Word;

import java.util.List;

public class VocabularyAdapter extends ArrayAdapter<Vocabulary> {

    public VocabularyAdapter(Context context, List<Vocabulary> wordList) {
        super(context, R.layout.vocabulary_list, wordList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Vocabulary wordList = getItem(position);
        WordsHolder wordsHolder;
        if (convertView == null) {
            wordsHolder = new WordsHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.vocabulary_list, parent, false);
            wordsHolder.word = (TextView) convertView.findViewById(R.id.vocabulary_word);
            convertView.setTag(wordsHolder);
        } else {
            wordsHolder = (WordsHolder) convertView.getTag();
        }
        String word = wordList.getWord();
        wordsHolder.word.setText(word);
        return convertView;
    }

    private class WordsHolder {

        public TextView word;
    }
}
