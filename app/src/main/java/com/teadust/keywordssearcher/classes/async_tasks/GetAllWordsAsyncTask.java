package com.teadust.keywordssearcher.classes.async_tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class GetAllWordsAsyncTask extends AsyncTask<Void, Void, List<String>> {

    private static final String PATTERN = "[,.\\s!?:;]";

    private String mSourceText;
    private List<String> mSourceWordsList;

    public GetAllWordsAsyncTask(String sourceText) {
        mSourceText = sourceText;
    }

    @Override
    protected List<String> doInBackground(Void... params) {
        initializeSourceWordsList();
        sortList();

        return mSourceWordsList;
    }

    private void initializeSourceWordsList() {
        Pattern pattern = Pattern.compile(PATTERN);
        String[] splitedText = pattern.split(mSourceText);
        mSourceWordsList = new ArrayList<String>();
        for (String word : splitedText) {
            if (word.length() > 1) {
                mSourceWordsList.add(removeQuotes(word));
            }
        }
    }

    private String removeQuotes(String word) {
        if (word.charAt(0) == '«' && word.charAt(word.length() - 1) == '»') {
            word = word.substring(1, word.length() - 1);
        } else if (word.charAt(0) == '«') {
            word = word.substring(1);
        } else if (word.charAt(word.length() - 1) == '»') {
            word = word.substring(0, word.length() - 1);
        }
        return word;
    }

    /**
     * Sorting list from A to z.
     */
    private void sortList() {
        Collections.sort(mSourceWordsList);
    }

    private void printListInLog() {
        for (String word : mSourceWordsList) {
            Log.d("ALLW", "List<String>: " + word);
        }
    }
}
