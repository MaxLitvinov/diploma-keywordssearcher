package com.teadust.keywordssearcher.classes.async_tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GetWordsMapAsyncTask extends AsyncTask<Void, Void, LinkedHashMap<String, Integer>> {

    private LinkedHashMap<String, Integer> mWordsMap;
    private List<String> mSourceList;

    public GetWordsMapAsyncTask(List<String> sourceList) {
        mWordsMap = new LinkedHashMap<String, Integer>();
        mSourceList = sourceList;
    }

    @Override
    protected LinkedHashMap<String, Integer> doInBackground(Void... params) {
        initializeAnalyseWordsMap();

        return mWordsMap;
    }

    private void initializeAnalyseWordsMap() {
        for (String word : mSourceList) {
            if (!mWordsMap.containsKey(word)) {
                mWordsMap.put(word, 1);
            } else {
                mWordsMap.put(word, mWordsMap.get(word) + 1);
            }
        }
    }

    private void printMapInLog() {
        for (Map.Entry entry : mWordsMap.entrySet()) {
            Log.d("SMAP", "LinkedHashMap<String, Integer>: " + entry.getKey() + ": " + entry.getValue());
        }
    }
}
