package com.teadust.keywordssearcher.classes.async_tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ParticleTask extends AsyncTask<Void, Void, LinkedHashMap<String, Integer>> {

    private static String[] particles = {"а", "ага", "аж", "ах",
            "б", "бишь", "бы", "было", "быть", "без", "был", "была",
            "в", "ведь", "вишь", "во", "вон", "вот", "вот-вот", "все", "всё", "всё-таки", "всего", "всю", "вы", "вам", "вас",
            "да", "даже", "де", "до",
            "едва", "если", "еще", "ещё", "его", "ее", "её",
            "ж", "же",
            "за", "из", "из-за",
            "и", "или", "иль", "именно", "имхо", "инда", "ить", "ишь", "их", "им", "ими",
            "к", "кажется", "как", "ко", "кому", "какой", "какого", "кто", "куда", "какая", "какие", "кого", "кем",
            "ладно", "ли", "лих", "лишь", "лучше", "ль",
            "мол", "ми", "мне", "меня", "мной", "мои", "мой",
            "на", "но", "не", "нее", "ней", "не-а", "небось", "нет", "нет-нет", "ни", "никак", "ну", "над", "нам", "нами", "нас", "наш",
            "об", "он", "от", "отколь", "откуда", "откудова", "отож", "очевидно", "она", "они", "оно",
            "по", "под", "поди", "пожалуй", "пожалуйста", "пока", "просто", "при", "про",
            "разве",
            "с", "се", "собственно", "спасибо", "со",
            "та", "так", "таки", "типа", "те", "тех", "то", "то-то", "тоже", "только", "тот", "там", "твои", "твой", "твое", "твоё", "твою", "твоя", "твоих", "тебе", "тебя", "ты", "ту", "тут", "туда", "том", "тому", "того", "тобой",
            "у", "уж", "уже", "ужели", "ужель", "ужли", "угу",
            "хорошо", "хоть", "хотя",
            "что", "что-то", "чтоб", "чтобы", "чуть", "чего", "чем",
            "эдак", "эк", "этак", "это", "эта", "эти", "этот", "этом", "этому", "эту", "этим", "этих", "этого", "этой",
            "якобы"
    };

    private LinkedHashMap<String, Integer> mSourceMap;
    private List<Integer> mQuantityList;

    public ParticleTask(LinkedHashMap<String, Integer> sourceMap, List<Integer> quantityList) {
        mSourceMap = sourceMap;
        mQuantityList = quantityList;
    }

    @Override
    protected LinkedHashMap<String, Integer> doInBackground(Void... params) {
        LinkedHashMap<String, Integer> mapWithoutParticles = removeParticles(mSourceMap);

        return mapWithoutParticles;
    }

    private LinkedHashMap<String, Integer> removeParticles(LinkedHashMap<String, Integer> sourceMap) {
        Iterator iterator = sourceMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> item = (Map.Entry<String, Integer>) iterator.next();
            for (String particle : particles) {
                if (particle.equalsIgnoreCase(item.getKey())) {
                    iterator.remove();
                }
            }
        }
        return sourceMap;
    }

    private void printMapInLog(LinkedHashMap<String, Integer> sourceMap) {
        for (Map.Entry entry : sourceMap.entrySet()) {
            Log.d("MAPWP", entry.getKey() + ": " + entry.getValue());
        }
    }
}
