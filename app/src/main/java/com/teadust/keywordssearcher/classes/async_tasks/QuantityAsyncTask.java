package com.teadust.keywordssearcher.classes.async_tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class QuantityAsyncTask extends AsyncTask<Void, Void, List<Integer>> {

    private static final String TAG_NAME = QuantityAsyncTask.class.getName();

    private LinkedHashMap<String, Integer> mSourceMap;

    public QuantityAsyncTask(LinkedHashMap<String, Integer> sourceMap) {
        mSourceMap = sourceMap;
    }

    @Override
    protected List<Integer> doInBackground(Void... params) {
        Set<Integer> quantitySet = getSet(mSourceMap);
        List<Integer> quantityList = getSortDownList(quantitySet);
//        for (Integer integer : quantityList) {
//            Log.d("TAGQL", "Quantity: " + integer);
//        }

        return quantityList;
    }

    private Set<Integer> getSet(Map<String, Integer> sourceMap) {
        Set<Integer> quantitySet = new HashSet<>();
        for (Map.Entry<String, Integer> entry : sourceMap.entrySet()) {
            quantitySet.add(entry.getValue());
        }
        return quantitySet;
    }

    private List<Integer> getSortDownList(Set<Integer> sourceSet) {
        List<Integer> quantityList = new ArrayList<Integer>(sourceSet);
        Collections.sort(quantityList);
        Collections.reverse(quantityList);
        return quantityList;
    }
}
