package com.teadust.keywordssearcher.classes.async_tasks;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ResultMapListAsyncTask extends AsyncTask<Void, Void, LinkedHashMap<Integer, List<String>>>{

    private static String[] particles = {"а", "ага", "аж", "ах",
            "б", "бишь", "бы", "было", "быть", "без", "был", "была", "были",
            "в", "ведь", "вишь", "во", "вон", "вот", "вот-вот", "все", "всё", "всё-таки", "всего", "всю", "вы", "вам", "вас",
            "да", "даже", "де", "до",
            "едва", "если", "еще", "ещё", "его", "ее", "её", "ему",
            "ж", "же",
            "за", "из", "из-за",
            "и", "или", "иль", "именно", "имхо", "инда", "ить", "ишь", "их", "им", "ими",
            "к", "кажется", "как", "ко", "кому", "какой", "какого", "кто", "куда", "какая", "какие", "кого", "кем", "когда",
            "ладно", "ли", "лих", "лишь", "лучше", "ль", "для",
            "мол", "ми", "мне", "меня", "мной", "мои", "мой",
            "на", "но", "не", "нее", "ней", "не-а", "небось", "нет", "нет-нет", "ни", "никак", "ну", "над", "него", "нам", "нами", "нас", "наш",
            "об", "он", "от", "отколь", "откуда", "откудова", "отож", "очевидно", "она", "они", "оно",
            "по", "под", "поди", "пожалуй", "пожалуйста", "пока", "просто", "при", "про",
            "разве",
            "с", "се", "собственно", "спасибо", "со", "снова",
            "та", "так", "таки", "типа", "те", "тех", "то", "то-то", "тоже", "только", "тот", "там", "твои", "твой", "твое", "твоё", "твою", "твоя", "твоих", "тебе", "тебя", "ты", "ту", "тут", "туда", "том", "тому", "того", "тобой",
            "у", "уж", "уже", "ужели", "ужель", "ужли", "угу",
            "хорошо", "хоть", "хотя",
            "что", "что-то", "чтоб", "чтобы", "чуть", "чего", "чем",
            "эдак", "эк", "этак", "это", "эта", "эти", "этот", "этом", "этому", "эту", "этим", "этих", "этого", "этой",
            "якобы"
    };

    private LinkedHashMap<String, Integer> mSourceMap;
    private List<Integer> mQuantityList;

    public ResultMapListAsyncTask(LinkedHashMap<String, Integer> sourceMap, List<Integer> quantityList) {
        mSourceMap = sourceMap;
        mQuantityList = quantityList;
    }

    @Override
    protected LinkedHashMap<Integer, List<String>> doInBackground(Void... params) {
        removeParticles(mSourceMap);
        LinkedHashMap<Integer, List<String>> resultMap = getResultMap();
//        removeEmptyValueKey(resultMap);

        printMapInLog(resultMap);

        return resultMap;
    }

    private LinkedHashMap<Integer, List<String>> getResultMap() {
        LinkedHashMap<Integer, List<String>> resultMap = new LinkedHashMap<Integer, List<String>>();
        String words = "";
        List<String> wordsList;
        for (Integer number : mQuantityList) {
            wordsList = new ArrayList<String>();
            for (Map.Entry entry : mSourceMap.entrySet()) {
                if (entry.getValue() == number) {
//                    words += entry.getKey() + " ";
                    wordsList.add(entry.getKey().toString());
                }
            }
            resultMap.put(number, wordsList);
//            words = "";
        }
        return resultMap;
    }

    private LinkedHashMap<String, Integer> removeParticles(LinkedHashMap<String, Integer> sourceMap) {
        LinkedHashMap<String, Integer> resultMap = null;
        Iterator iterator = sourceMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> item = (Map.Entry<String, Integer>) iterator.next();
            for (String particle : particles) {
                if (particle.equalsIgnoreCase(item.getKey())) {
                    iterator.remove();
                }
            }
        }
        resultMap = sourceMap;
        return resultMap;
    }

    private LinkedHashMap<Integer, List<String>> removeEmptyValueKey(LinkedHashMap<Integer, List<String>> sourceMap) {
        Iterator iterator = sourceMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, String> item = (Map.Entry<Integer, String>) iterator.next();
            if (item.getValue().length() < 1) {
                iterator.remove();
            }
        }
        return sourceMap;
    }

    private void printMapInLog(LinkedHashMap<Integer, List<String>> sourceMap) {
        for (Map.Entry entry : sourceMap.entrySet()) {
            List<String> list = (List<String>) entry.getValue();
            Log.d("RMP", "------------------------------------------start");
            for (String str : list) {
                Log.d("RMP", entry.getKey() + ": " + str);
            }
            Log.d("RMP", "------------------------------------------end");
        }
    }
}
