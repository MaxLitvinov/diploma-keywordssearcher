package com.teadust.keywordssearcher.classes.async_tasks;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

public class StorageFileAsyncTask extends AsyncTask<String, String, String> {

    private static final String TAG_CLASS_NAME = StorageFileAsyncTask.class.getName();

    private String mFullFilePath;
    private String mFileName;
    private String mFilePath;

    private Context mContext;
    private Uri mUri;
    private TextView mTextView;

    public StorageFileAsyncTask(Context context, Uri uri, TextView textView) {
        mContext = context;
        mUri = uri;
        mTextView = textView;
        mTextView.setText("");
    }

    @Override
    protected void onPreExecute() {
        try {
            getFullFilePath();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String documentText = "";

        File file = new File(mFilePath, mFileName);
        byte[] text = new byte[getFileSize(new File(mFilePath, mFileName))];
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            int bytesRead = 0;
            String line;
            while ((bytesRead = bufferedInputStream.read(text)) != -1) {
                line = new String(text, 0, bytesRead);
                publishProgress(line);
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG_CLASS_NAME, e.getMessage());
        } catch (IOException e) {
            Log.d(TAG_CLASS_NAME, e.getMessage());
        }

        return documentText;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        mTextView.append(values[0]);
    }

    public void getFullFilePath() throws URISyntaxException {
        if ("content".equalsIgnoreCase(mUri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = mContext.getContentResolver().query(mUri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    mFullFilePath = cursor.getString(column_index);
                }
            } catch (Exception e) {
                Log.d(TAG_CLASS_NAME, e.getMessage());
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        } else if ("file".equalsIgnoreCase(mUri.getScheme())) {
            mFullFilePath = mUri.getPath();
        }
        mFileName = getFileName(mFullFilePath);
        mFilePath = getFilePath(mFullFilePath);
    }

    private String getFileName(String fullFilePathWithName) {
        return fullFilePathWithName.substring(fullFilePathWithName.lastIndexOf(File.separator) + 1,
                fullFilePathWithName.length());
    }

    private String getFilePath(String fullFilePathWithName) {
        return fullFilePathWithName.substring(0, fullFilePathWithName.lastIndexOf(File.separator));
    }

    private static int getFileSize(File file) {
        int fileSize = (int) file.length();
        return fileSize;
    }
}
