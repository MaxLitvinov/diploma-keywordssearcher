package com.teadust.keywordssearcher.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.teadust.keywordssearcher.classes.Vocabulary;

import java.util.ArrayList;
import java.util.List;

public class VocabularyDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "VocabularyDatabase";

    private static class Column implements BaseColumns {
        public static final String TABLE_NAME = "table_vocabulary";
        public static final String ID = "id";
        public static final String WORD = "word";
        public static final String[] ROW = { ID, WORD };
    }

    private static class Query {
        public static final String CREATE_TABLE = "CREATE TABLE " + Column.TABLE_NAME
                + " (" + Column.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Column.WORD + " TEXT);";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + CREATE_TABLE;
        public static final String GET_ALL_NOTES = "SELECT * FROM " + Column.TABLE_NAME;
    }

    public VocabularyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public String getDatabaseName() {
        return DATABASE_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Query.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Query.DROP_TABLE);
    }

    public long addWord(Vocabulary vocabulary) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Column.WORD, vocabulary.getWord());

        long addedWord = db.insert(Column.TABLE_NAME, null, values);

        db.close();

        return addedWord;
    }

    public void deleteWord(long id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Column.TABLE_NAME, Column.ID + "=?",
                new String[]{String.valueOf(id)});

        db.close();
    }

    public Vocabulary getWord(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                Column.TABLE_NAME,
                Column.ROW,
                Column.ID + "=?",
                new String[] { String.valueOf(id) },
                null, null, null, null);

        Vocabulary vocabulary = new Vocabulary( );
        if (cursor != null) {
            cursor.moveToFirst();
            vocabulary.setId(Integer.parseInt(cursor.getString(0)));
            vocabulary.setWord(cursor.getString(1));
            cursor.close();
        }

        return vocabulary;
    }

    public Vocabulary getLastAddedNote() {
        long notesCount = getWordsCount();
        return getWord(notesCount);
    }

    public List<Vocabulary> getAllWords() {
        List<Vocabulary> wordsList = new ArrayList<Vocabulary>();

        String selectQuery = "SELECT * FROM " + Column.TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Vocabulary vocabulary = new Vocabulary();
                vocabulary.setId(c.getInt(c.getColumnIndex(Column.ID)));
                vocabulary.setWord(c.getString(c.getColumnIndex(Column.WORD)));
                wordsList.add(vocabulary);
            } while (c.moveToNext());
        }

        c.close();

        return wordsList;
    }

    public long getWordsCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(Query.GET_ALL_NOTES, null);
        long wordsCount = cursor.getCount();
        cursor.close();

        return wordsCount;
    }
}
