package com.teadust.keywordssearcher.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.teadust.keywordssearcher.R;

public class AnalyseTextTwoFragment extends Fragment {

    private ListView mListView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_analyse_text_two, container, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.listView = (ListView) rootView.findViewById(R.id.list_view);

        mListView = viewHolder.listView;

        return rootView;
    }

    private class ViewHolder {

        public ListView listView;
    }

    public ListView getListView() {
        return mListView;
    }
}
