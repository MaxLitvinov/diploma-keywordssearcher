package com.teadust.keywordssearcher.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teadust.keywordssearcher.R;

public class FirstSourceTextFragment extends Fragment {

    private TextView mTextView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_first_source_text, container, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.sourceText = (TextView) rootView.findViewById(R.id.source_text);

        mTextView = viewHolder.sourceText;

        return rootView;
    }

    private class ViewHolder {

        public TextView sourceText;
    }

    public TextView getTextView() {
        return mTextView;
    }
}
