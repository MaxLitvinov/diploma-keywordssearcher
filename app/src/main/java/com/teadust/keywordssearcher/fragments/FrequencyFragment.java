package com.teadust.keywordssearcher.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.teadust.keywordssearcher.R;

public class FrequencyFragment extends Fragment {

    private ListView mListViewOne;
    private ListView mListViewTwo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        int index = mListView.getFirstVisiblePosition();
//        View view = mListView.getChildAt(0);
//        int top = (view == null) ? 0 : view.getTop();
//
//        outState.putInt("index", index);
//        outState.putInt("top", top);
//
//        Log.d("TAG", "index: " + index + ", top: " + top);
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        if (savedInstanceState != null) {
//            int index = savedInstanceState.getInt("index", -1);
//            int top = savedInstanceState.getInt("top", 0);
//
//            if (index != -1) {
//                mListView.setSelectionFromTop(index, top);
//            }
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_frequency, container, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.listViewOne = (ListView) rootView.findViewById(R.id.list_view_text_one);
        viewHolder.listViewTwo = (ListView) rootView.findViewById(R.id.list_view_text_two);

        mListViewOne = viewHolder.listViewOne;
        mListViewTwo = viewHolder.listViewTwo;

        return rootView;
    }

    private class ViewHolder {

        public ListView listViewOne;
        public ListView listViewTwo;
    }

    public ListView getListViewOne() {
        return mListViewOne;
    }

    public ListView getListViewTwo() {
        return mListViewTwo;
    }
}
